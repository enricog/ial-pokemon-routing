import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Page1Component } from './components/page1/page1.component';
import { Page2Component } from './components/page2/page2.component';
import { ListaComponent } from './components/lista/lista.component';
import { DettaglioComponent } from './components/dettaglio/dettaglio.component';

const routes: Routes = [
  {
    path: 'lista',
    component: ListaComponent
  },
  {
    path: 'lista/:id',
    component: DettaglioComponent
  },
  {
    path: 'pagina1',
    component: Page1Component
  },
  {
    path: 'pagina2',
    component: Page2Component
  },
  {
    path: 'pagina2/:prova',
    component: Page2Component
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
