import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    
    private baseUrl = "https://pokeapi.co/api/v2/pokemon/";

    constructor(private http: HttpClient) {
        
    }

    getPokemons(){
        return this.http.get<any>(this.baseUrl); //.pipe(map(x => x.results))
    }

    getPokemon(id: string){
        return this.http.get<any>(this.baseUrl + id);
    }


}