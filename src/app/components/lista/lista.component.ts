import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-lista',
  template: `
    <ul>
      <li *ngFor="let item of list">
        <a routerLink="/lista/{{item.name}}">{{item.name}}</a>
      </li>
    </ul>
  `,
  styles: []
})
export class ListaComponent implements OnInit {

  list: any[];
  //list$: Observable<any>;

  constructor(public api: ApiService) { 
    this.api.getPokemons().subscribe(x => {
      this.list = x.results;
    });


    //this.list$ = api.getPokemons();
  }

  ngOnInit() {
  }

}
