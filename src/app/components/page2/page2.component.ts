import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-page2',
  template: `
  <h1>INVECE IO SONO LA PAGINA 2</h1>
  `,
  styles: []
})
export class Page2Component implements OnInit {

  constructor(private route: ActivatedRoute) {
    console.log(this.route.snapshot.params);

    // this.route.params.subscribe(x => {
      
    //   console.log(x);
    // })
   }

  ngOnInit() {
  }

}
