import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-dettaglio',
  template: `
  <div *ngIf="pokemon">
    <h1>Nome: {{pokemon.name}}</h1>
    <h4>Peso: {{pokemon.weight}}</h4>
    <h4>Altezza: {{pokemon.height}}</h4>
    <ul>
    <li *ngFor="let a of pokemon.abilities">{{a.ability.name}}</li>
    </ul>
  </div>
  `,
  styles: []
})
export class DettaglioComponent implements OnInit {

  pokemon: any;


  constructor(private route: ActivatedRoute, private api: ApiService) { 
    const pokename = route.snapshot.params.id;
    api.getPokemon(pokename).subscribe(x => {
      this.pokemon = x;
      console.log(this.pokemon);
    });
  }

  ngOnInit() {
  }

}
